ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG HIVE_VERSION
ENV HIVE_VERSION $HIVE_VERSION

ARG HADOOP_VERSION
ENV HADOOP_VERSION $HADOOP_VERSION

LABEL maintainer="gitlab@therack.io"

# Setup
#RUN \
#    apt -qq -y update

# Install Hadoop 
ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/apache-hadoop-bin-arm64/$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz /
RUN \
    cd / && \
    tar -xzf /hadoop-$HADOOP_VERSION.tar.gz && \
    rm -rf /hadoop-$HADOOP_VERSION.tar.gz && \
    ls -al && \
    ls -al /hadoop-$HADOOP_VERSION/lib/ && \
    pwd && \
    ln -s /hadoop-$HADOOP_VERSION /opt/hadoop && \
    ls -al /opt/hadoop/lib && \
    ls -al /opt/hadoop/lib/native

ENV HADOOP_HOME="/opt/hadoop"
ENV HADOOP_OPTS="${HADOOP_OPTS} -Djava.library.path=${HADOOP_HOME}/lib/native"
ENV LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HADOOP_HOME}/lib/native"
ENV PATH="${HADOOP_HOME}/bin:${HADOOP_HOME}/sbin:${PATH}"

RUN \
    ldconfig && \
    hadoop version && \
    hadoop classpath

# Install Hive 
ADD https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/apache-hive-bin-arm64/$HIVE_VERSION/apache-hive-$HIVE_VERSION.tar.gz /
RUN \
    cd / && \
    tar -xzf /apache-hive-$HIVE_VERSION.tar.gz && \
    rm -rf /apache-hive-$HIVE_VERSION.tar.gz && \
    ls -al && \
    ls -al /apache-hive-$HIVE_VERSION-bin && \
    pwd && \
    ln -s /apache-hive-$HIVE_VERSION-bin /opt/hive && \
    ls -al /opt/hive && \
    ls -al /opt/hive/lib && \
    ls -al /opt/hive/bin && \
    ls -al /opt/hive/jdbc

ENV HIVE_HOME="/opt/hive"
ENV PATH="${HIVE_HOME}/bin:${PATH}"
ENV CLASSPATH="${CLASSPATH}:${HIVE_HOME}/lib/*"
ENV HIVE_CLASSPATH="${HIVE_HOME}/lib/*"

# Remove Hive SLF4J - https://issues.apache.org/jira/browse/HIVE-6162
RUN rm -rf /opt/hive/lib/log4j*.jar

RUN hive --version
